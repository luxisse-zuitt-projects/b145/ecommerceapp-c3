import express from 'express';
import bcrypt from 'bcryptjs';
import expressAsyncHandler from 'express-async-handler';
import User from '../models/userModel.js';
import { generateToken } from '../auth.js';



const userRouter = express.Router();

// login route
userRouter.post(
    '/login', 
    expressAsyncHandler(async (req, res) => {
        // check if user exists
        const user = await User.findOne({ email: req.body.email });
        if (user) {
            // check if password matches
            if (bcrypt.compareSync(req.body.password, user.password)) {
                res.send({
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    isAdmin: user.isAdmin,
                    token: generateToken(user)
                });
                return;
            }
        }
        // invalid email or password
        res.status(401).send({ message: "Sorry, that email or password isn't right" });
    })
);

// register route
userRouter.post(
    '/register', 
    expressAsyncHandler(async (req, res) => {
        const newUser = new User({
            name: req.body.name,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password)
        });
        
        // save new user to database
        const user = await newUser.save();
        
        // send user data to frontend
        res.send({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user)
        });
    })
);

export default userRouter;