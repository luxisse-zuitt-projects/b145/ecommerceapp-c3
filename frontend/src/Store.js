import { createContext, useReducer } from 'react';



export const Store = createContext();

const initialState = {

    userInfo: localStorage.getItem('userInfo')
    ? JSON.parse(localStorage.getItem('userInfo')) 
    : null ,
    
    cart: {

        shippingDetails: localStorage.getItem('shippingDetails')
        ? JSON.parse(localStorage.getItem('shippingDetails'))
        : {},

        paymentMethod: localStorage.getItem('paymentMethod')
        ? localStorage.getItem('paymentMethod')
        : '',

        cartItems: localStorage.getItem('cartItems')
        ? JSON.parse(localStorage.getItem('cartItems'))
        : [],

    }
};

function reducer(state, action) {
    switch(action.type) {
        case 'CART_ADD_ITEM':
        //add to cart
        const newItem = action.payload;
        const existItem = state.cart.cartItems.find(
            (item) => item._id === newItem._id
        );

        const cartItems = existItem
        ? state.cart.cartItems.map((item) =>
            item._id === existItem._id ? newItem : item
        )
        : [...state.cart.cartItems, newItem];
        
        // save to local storage
        localStorage.setItem('cartItems', JSON.stringify(cartItems));
        
        return { ...state, cart: {...state.cart, cartItems} };
        
        // remove from cart
        case 'CART_REMOVE_ITEM': {
            const cartItems = state.cart.cartItems.filter(
                (item) => item._id !== action.payload._id
            );

            // save to local storage
            localStorage.setItem('cartItems', JSON.stringify(cartItems));

        return { ...state, cart: {...state.cart, cartItems} };
        }

        // user login
        case 'USER_LOGIN':
            return { ...state, userInfo: action.payload };

        // user logout
        case 'USER_LOGOUT':
            return { 
                ...state, 
                userInfo: null,
                cart: {
                    cartItems: [],
                    shippingDetails: {},
                    paymentMethod: ''
                }
            };
        
        // save shipping details
        case 'SAVE_SHIPPING_DETAILS':
            return {
                ...state,
                cart: {
                    ...state.cart,
                    shippingDetails: action.payload
                }
            };

        // save payment method
        case 'SAVE_PAYMENT_METHOD':
            return {
                ...state,
                cart: {
                    ...state.cart,
                    paymentMethod: action.payload
                }
            };

        default:
            return state;
    }
};

export function StoreProvider(props) {
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = {state, dispatch};
    return <Store.Provider value={value}> {props.children} </Store.Provider>;
};