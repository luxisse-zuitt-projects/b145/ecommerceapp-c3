import { Link, useLocation, useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { useContext, useEffect, useState } from "react";
import { Store } from '../Store';
import { toast } from "react-toastify";
import { getError } from "../utils";
import Axios from 'axios';
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";



export default function Login() {

    const navigate = useNavigate();
    const { search } = useLocation();
    const redirectInUrl = new URLSearchParams(search).get('redirect');
    const redirect = redirectInUrl ? redirectInUrl : "/";

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const { state, dispatch: ctxDispatch } = useContext(Store);
    const { userInfo } = state;

    const submitHandler = async (e) => {
        e.preventDefault();
        try {
            const { data } = await Axios.post('/api/users/login', {
                email,
                password
            });
            // successful login
            ctxDispatch({type: 'USER_LOGIN', payload: data})
            localStorage.setItem('userInfo', JSON.stringify(data));
            // redirect to url or home (if page doesn't exist)
            navigate(redirect || '/');
        } catch (err) {
            {/* custom notif */}
            toast.error(getError(err));
        }
    };

    // Registration/Login pages must be inaccessible to logged-in users. Logged-in users will be redirected to homepage.
    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [navigate, redirect, userInfo]);

    return (
        <Container className="small-container">
            <Helmet>
                <title>
                    Log In
                </title>
            </Helmet>

            <h1 className="our-products">Log in to your account</h1>

            <div className="div-form">
            <Form className="login-form" onSubmit = {submitHandler}>
                
                {/* Email */}
                <Form.Group className = "mb-3" controlId = "email">
                    <Form.Label> Email </Form.Label>
                    <Form.Control type = "email" required onChange={(e) => setEmail(e.target.value)} />
                </Form.Group>

                {/* Password */}
                <Form.Group className = "mb-3" controlId = "password">
                    <Form.Label> Password </Form.Label>
                    <Form.Control type = "password" required onChange={(e) => setPassword(e.target.value)} />
                </Form.Group>

                {/* Login btn */}
                <div className="mb-3 mt-5">
                    <Button className="form-btn" type="submit">Log In</Button>
                </div>

                <div className="caption">
                    New to our shop? {' '}
                    <Link className="cart-empty" to = {`/register?redirect=${redirect}`}>Create your account</Link>
                </div>
            </Form>
            </div>
        </Container>
    );
};