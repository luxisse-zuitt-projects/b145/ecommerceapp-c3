import { Link, useLocation, useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet-async";
import { useContext, useEffect, useState } from "react";
import { Store } from '../Store';
import { toast } from "react-toastify";
import { getError } from "../utils";
import Axios from 'axios';
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";



export default function Register() {

    const navigate = useNavigate();
    const { search } = useLocation();
    const redirectInUrl = new URLSearchParams(search).get('redirect');
    const redirect = redirectInUrl ? redirectInUrl : "/login";

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const { state, dispatch: ctxDispatch } = useContext(Store);
    const { userInfo } = state;

    const submitHandler = async (e) => {
        e.preventDefault();

        // if passwords don't match
        if(password !== confirmPassword) {
            toast.error('Passwords do not match');
            return;
        }

        try {
            const { data } = await Axios.post('/api/users/register', {
                name,
                email,
                password
            });
            // successful login
            ctxDispatch({type: 'USER_LOGIN', payload: data})
            localStorage.setItem('userInfo', JSON.stringify(data));
            // redirect to url or home (if page doesn't exist)
            navigate(redirect || '/login');
        } catch (err) {
            {/* custom notif */}
            toast.error(getError(err));
        }
    };


    // Registration/Login pages must be inaccessible to logged-in users. Logged-in users will be redirected to homepage.
    useEffect(() => {
        if (userInfo) {
            navigate(redirect);
        }
    }, [navigate, redirect, userInfo]);

    return (
        <Container className="small-container">
            <Helmet>
                <title>
                    Register
                </title>
            </Helmet>

            <h1 className="our-products">Create your account</h1>

            <div className="div-form">
            <Form className="login-form" onSubmit = {submitHandler}>
                
                {/* Name */}
                <Form.Group className = "mb-3" controlId = "name">
                    <Form.Label> Name </Form.Label>
                    <Form.Control onChange={(e) => setName(e.target.value)} required />
                </Form.Group>

                {/* Email */}
                <Form.Group className = "mb-3" controlId = "email">
                    <Form.Label> Email </Form.Label>
                    <Form.Control type = "email" required onChange={(e) => setEmail(e.target.value)} />
                </Form.Group>

                {/* Password */}
                <Form.Group className = "mb-3" controlId = "password">
                    <Form.Label> Password </Form.Label>
                    <Form.Control type = "password" required onChange={(e) => setPassword(e.target.value)} />
                </Form.Group>

                {/* Confirm Password */}
                <Form.Group className = "mb-3" controlId = "confirmPassword">
                    <Form.Label> Confirm Password </Form.Label>
                    <Form.Control 
                        type = "password" 
                        required 
                        onChange={(e) => setConfirmPassword(e.target.value)} />
                </Form.Group>

                {/* Register btn */}
                <div className="mb-3 mt-5">
                    <Button className="form-btn" type="submit">Create account</Button>

                    {/* <Link to="/login" className="register-btn">Create account</Link> */}
                </div>

                {/* Link to Login */}
                <div className="caption">
                    Already have an account? {' '}
                    <Link className="cart-empty" to = {`/login?redirect=${redirect}`}>Log in to your account</Link>
                </div>
            </Form>
            </div>
        </Container>
    );
};